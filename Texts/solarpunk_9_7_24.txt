
Hedonismus & Solarpunk
----------------------

Sex Drugs & Rock n Roll meets Solarpunk?

====

Prickel Prickel, ist auch ein Tag im Discordianischen Kalender.
An einem heissen Sommertag, ist das Prickeln des Kaltgetränks zusammen mit einem dunkel-muffig-kühlen Raum,
jedoch die reinste Wohltat.

Das prickelnde rieseln wird leiser - erinnert an nieseln.

Nach einem Fahrradritt mit kühlendem Fahrtwind, kühlen jetzt Gedanken den Kopf.

Sind Gedanken schon Stimulation?
Kann man sich neben der repetiven, wütenden Destruktion auch in Extase denken?

===

Überflussdenken <-> Suffizienz

Entschleunigung -> Zeit für schönes

Wohlgefühl alleine

Konsumfreiheit: Freiheit zum Konsum
	Freiheit vom Konsum

Spiritualität


-----> überwinden der Klimakriese muss in neuen Gesellschaftsvertrag gegen die Wiederholbarkeit der Klimakrise

Gibt es im Solarpunk ein Desert

Desert: Gesckmack, Kultur, Genuss, Estetik

Der Pferdefuss an Substanzengebrauch

Drogen als Medizin/Werkzeug?

Solarpunk: nüchterne, erreichbare Utopie

Ästhetik, Kultur, Feiern; Wertschätzung (Gönnung)

Forschritt, Neugierde vs. Suffizienz, in sich ruhend

Kein Vaterschaftstest -> Alle kümmern sich

===

Familiengeschichte
========================


Note: ernüchternder Smily
Sonne Baum, Mond

Schweinchen das mahlzeit haben möchte (Zugabteil)

Decke(Solarpanel)


Der Mond war schon durchs Fenster zu sehen, dennoch war es heiss.
Die Sonne war sengend gewesen. Trotzdem war die Stimmung im Abteil ausgelassen.
Etwas anstrengend, aber mitreissend.
Viele waren oben ohne, um die Temperaturen besser ertragen zu können.
In der Mitte saß eine Oma mit Fächer, die mit kunterbunten Geschichten die Aufmerksamkeit, nicht nur der Kinder fesselte.
Es ging eine Ukulele herum, die mit dem Soundsystem im Abteil gekoppelt war und ebenso sehr in Klängen schillerte, wie die Geschichten.

Daneben gab es regen Austausch, über strategien konstruktiver digitaler Kommunikation in der Elfenlegion.
Kreative Methoden zum Perspektiven Wechsel bei technischen Herausforderung oder schlicht die Kunst immer gedeihende Flaschen-Terrarien zu bauen.
Man kam sich näher. Manch einer verschwand auf den Schlafplatz, gegenüber der Gepäckablage im Abteil.

Zwei von den jugendlichen blickten Kopf an Kopf, Arm in Arm darauf, wie der Sonnenuntergang sich in den Solarpanelen spiegelte, die draussen vorbeizogen.
Bald würde sich die Landschaft in kühle bewaldete Berge wandeln.


===============
Austausch - Klimacamp - Abteil
==========

Saatbombe
---------

Masse, statt klasse, ein Spiel mit dem Zufall.

Schwupp eine Saatbombe aus dem Fahrradkorb geangelt und an den Straßenrand geworfen.

Vielleicht fühlt sich der Samen da wohl und in einem Monat würde man schon sehen, ob es sprießt oder nicth.
Organisch. Stochastisches Gärtnern.

Saatbomben sind wie ein Keksrezept, an dem man stetig feilt.
Ein kleines Ökosystem, aus Flechten, Moos und eingenistet in Dünger und Erdreich verschiedene Samen, die sich stützen und ergänzen.
Es wahrscheinlicher machen, dass doch etwas wächst.

Manche Orte entwickeln sich über die Zeit, werden zu wilden kleinen Beeten, die sich selbst über Jahre erhalten und weiter ausbreiten.
So geht es mit den Rezepten für Saatbomen, die getauscht werden.

In manchen wilden Beeten findet man auf kleinen bunten Scherben auch den Hinweis, die Information.
Wer wann, Ideen ersann.. die hinauswachsen aus dem scheinbar willkürlichen Grün am Straßenrand.

--------------------------
Hausaufgabe
---------------

Baumhaus, Hängematte, Spazierstock, Handabdruck, Fahradfahren, Musik/Note
 Familien

---------------
(sexuell) bewahren von neugierde; unsicherheit, offenheit; erfahrung als gewissheit etwas positives gestalten zu können
