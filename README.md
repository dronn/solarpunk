# SolarPunk

Dieses Repo enthält Logo, Flyer, Schriftarten und Texte die bei Schreibworkshops entstanden sind.
Die Texte finden sich auch im Fediverse: https://paper.wf/solarpunxaux/

Auch zu empfehlen: https://mastodon.social/tags/solarpunk

## Solarpunk Schreibworkshops laufen wie folgt ab:

### Einführung, was ist Solarpunk für Neue.
Ein Science Fiction Genre in dem der Klimawadel gesellschaftlich überwunden ist. Gegensatz zu Cyberpunk. Eine erreichbare Utopie.

### 5 Minuten freies, assoziatives Einschreiben
- reihum: wer möchte liest vor

### Ziehen eines zufälligen Solarpunk [Begriffe](/Texts/solarpunkWords.txt) aus einem Glas

Schreibe 5 Minuten zum gezogenen [Begriff](/Texts/solarpunkWords.txt)
- reihum: wer möchte liest vor


### Optionaler Einschub: Diskussion zu einem Thema:
- Kann man Familie anders im Solarpunk denken: Eine ganze Community erzieht ein Kind; Das Leben findet auf der Straße statt etc
- Positive Männlichkeitsentwürfe/ Gutes zwischengeschlechtliches Miteinander
- Funktionieren Helden(reisen) in Solarpunk und warum nicht
- Erstellt Charaktere und lasst sie in einer Geschichte aufeinandertreffen
- viele mehr

### 10 Minuten Solarpunk Storycubes
- 5 Würfel beklebt mit Symbolen aus ![Solarpunk Symbole](/Texts/symbols.png)
- Würfelt und beschreibt die Symbole
- Schreibt 10 Minuten lang eine Geschichte in der alle gewürfelten Symbole enthalten sind
- reihum: wer möchte liest vor

### 10 Minuten zu einem gezogenen Solarpunk [Begriffe](/Texts/solarpunkWords.txt)
- Mit Inspiration aus dem optionalen Einschub
- reihum: wer möchte liest vor

